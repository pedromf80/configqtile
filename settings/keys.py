# Qtile keybindings

from libqtile.config import Key
from libqtile.command import lazy


mod = "mod4"

keys = [Key(key[0], key[1], *key[2:]) for key in [
    # ------------ Window Configs ------------

    # Switch between windows in current stack pane
    ([mod], "j", lazy.layout.down()),
    ([mod], "k", lazy.layout.up()),
    ([mod], "h", lazy.layout.left()),
    ([mod], "l", lazy.layout.right()),

    # Change window sizes (MonadTall)
    ([mod, "shift"], "l", lazy.layout.grow()),
    ([mod, "shift"], "h", lazy.layout.shrink()),

    # Toggle floating
    ([mod, "shift"], "f", lazy.window.toggle_floating()),

    # Move windows up or down in current stack
    ([mod, "shift"], "j", lazy.layout.shuffle_down()),
    ([mod, "shift"], "k", lazy.layout.shuffle_up()),

    # Toggle between different layouts as defined below
    ([mod], "Tab", lazy.next_layout()),
    ([mod, "shift"], "Tab", lazy.prev_layout()),

    # Kill window
    ([mod], "w", lazy.window.kill()),

    # Switch focus of monitors
    ([mod], "period", lazy.next_screen()),
    ([mod], "comma", lazy.prev_screen()),

    # Restart Qtile
    ([mod, "control"], "r", lazy.restart()),

    ([mod, "control"], "q", lazy.shutdown()),
    ([mod], "r", lazy.spawncmd()),

    # ------------ App Configs ------------
    # Menu
    ([mod], "m", lazy.spawn("rofi -show drun")),

    # Window Nav
    ([mod, "shift"], "m", lazy.spawn("rofi -show")),

    # Browser
    ([mod], "b", lazy.spawn("brave")),
    ([mod], "f", lazy.spawn("firefox")),

    # File Explorer
    #([mod], "e", lazy.spawn("pcmanfm")),
    # ([mod], "e", lazy.spawn("thunar")),
     ([mod], "e", lazy.spawn("caja")),
    #([mod], "g", lazy.spawn("ranger")),
    
    # Terminal
    ([mod], "Return", lazy.spawn("alacritty")),
    #([mod], "x", lazy.spawn("xterm")),
    ([mod, "shift"], "k", lazy.spawn("kitty")),

    # Redshift
    ([mod], "r", lazy.spawn("redshift -O 5000")),
    ([mod, "shift"], "r", lazy.spawn("redshift -x")),

    # Pomodoro Productivity
    ([mod],"p", lazy.spawn("pomotroid")),
    
    # Screenshot
    ([mod], "s", lazy.spawn("scrot -s")),
    ([mod, "shift"], "s", lazy.spawn("scrot -u")),
    #([mod, "shift"], "d", lazy.spawn("scrot -D")),

    # code
    ([mod], "v", lazy.spawn("code")),
    ([mod], "t", lazy.spawn("pluma")),


    # ------------ Hardware Configs ------------

    # Volume
    ([], "XF86AudioLowerVolume", lazy.spawn(
       "pactl set-sink-volume @DEFAULT_SINK@ -5%"
    )),
    ([], "XF86AudioRaiseVolume", lazy.spawn(
        "pactl set-sink-volume @DEFAULT_SINK@ +5%"
    )),
    ([], "XF86AudioMute", lazy.spawn(
        "pactl set-sink-mute @DEFAULT_SINK@ toggle"
    )),

    # Brightness
        #extend monitor
    ([mod], "h", lazy.spawn("xrandr --output HDMI-1 --brightness 0.5")),
   
        #special button for brightness
    ([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl set +10%")),
    ([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl set 10%-")),
]]
